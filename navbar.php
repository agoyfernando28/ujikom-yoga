<?php 
include 'header.php';
?>

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
              <p>Agoy Fernando</p>

              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <ul class="nav side-menu">
                                <li><a href="index.php"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
                                <li><a href="user.php"><i class="fa fa-table"></i>&nbsp;Data User</a></li>
                                <li><a href="data_peminjaman.php"><i class="fa fa-share"></i>&nbsp;Data Peminjaman</a></li>
                                <li><a href="pengembalian.php"><i class="fa fa-fa fa-reply"></i> &nbsp;Pengembalian</a></li> 
                                <li><a href="laporan.php"><i class="fa fa-fa fa-book"></i> &nbsp;Laporan</a></li> 
                                
                        </div>
                    </div>
        <!-- /.sidebar -->
      </aside>

      <!-- =============================================== -->